import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class ClientInput extends Thread {

	InputStream is;
	Socket socket;
	ChatWindow cw;
	String username;

	public ClientInput(Socket socket, ChatWindow cw, String username) throws IOException {
		this.socket = socket;
		is = socket.getInputStream();
		this.cw = cw;
		this.username = username;
	}

	public void run() {
		while (!socket.isClosed()) {
			String s = new String();
			int b;
			try {
				b = is.read();
				s += (char) b;
				while (b != '\n' && b != -1) {
					b = is.read();
					s += (char) b;
				}
				System.out.println(s);
				cw.add(s);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		}
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getUsername() {
		if (!username.isEmpty()) {
			return "";
		} else {
			return username;
		}
	}
}